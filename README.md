# Use-Case Bodenfeuchte

## Projektbeschreibung

In diesem Use-Case wurde die Integration mehrere Bodenfeuchtesensoren durchgeführt und ein prototypisches Dashboard erstellt. Um für das Proof-of-Concept-Dashboard eine große Anzahl an Sensoren und Geolocations zu haben, wurden Dummy-Sensoren mit künstlich generierten Daten erstellt. Neben der Bodenfeuchte wird der Waldbrandgefahrenindex der DWD abgerufen und in das Dashboard integriert.

Der Use-Case wurde auf der FIWARE-basierten [Urban Dataspace Platform](https://gitlab.com/urban-dataspace-platform/core-platform) entwickelt und integriert.

![Grafana Dashboard](./docs/grafana_dashboard.png "Grafana Dashboard")

Die Messung und Überwachung der Bodenfeuchte in verschiedenen Gebieten einer Stadt und ihrer Umgebung ist aus mehreren Gründen wichtig. Erstens liefert sie wertvolle Erkenntnisse über die Bodenverhältnisse, die sich erheblich auf die städtische Landwirtschaft, Parks und Grünflächen innerhalb der Stadtgrenzen auswirken können. Eine angemessene Bodenfeuchte ist für das Wachstum und die Pflege von Pflanzen, Bäumen und städtischen Gärten unerlässlich und trägt zu einer grüneren und nachhaltigeren städtischen Umwelt bei.

Außerdem können Daten über die Bodenfeuchte helfen, potenzielle Dürrezustände oder Wasserknappheit frühzeitig zu erkennen. Durch die Analyse der Feuchtigkeitswerte im Zeitverlauf und an verschiedenen Orten können Stadtplaner und Wasserwirtschaftsbehörden fundierte Entscheidungen über Wassersparmaßnahmen, Bewässerungsstrategien und die Umsetzung von Wassersparinitiativen treffen.

Darüber hinaus können Bodenfeuchtedaten mit anderen Umweltsensoren und georeferenzierten Systemen integriert werden, was ein umfassendes Verständnis des städtischen Mikroklimas und effektivere Entscheidungsprozesse ermöglicht.

## Installationsanleitung

Alle für den Anwendungsfall benötigten Resourcen sind in dem Gitlab.com-Projekt https://gitlab.com/urban-dataspace-platform/use_cases/integration/bodenfeuchte enthalten. 

Um den Anwendungsfall zu installieren sind folgende Schritte nötig:  
1. Eine lokale Maschine für die Installation ist vorzubereiten (Siehe https://gitlab.com/urban-dataspace-platform/core-platform/-/blob/master/00_documents/Admin-guides/cluster.md).
2. Das [Deployment-Projekt](https://gitlab.com/urban-dataspace-platform/use_cases/integration/bodenfeuchte/deployment) muss auf die lokale Maschine "geclont" werden (rekursiv!).
3. Die im Deployment beschriebenen Schritte ausführen.

## Gebrauchsanweisung

Der NodeRED-Flow erhält einerseits echte Daten über LoRaWAN (MQTT), andererseits generiert er künstliche Daten. Diese Daten werden in das NGSI-kompatible [GreenspaceRecord Smart-Data-Model](https://github.com/smart-data-models/dataModel.ParksAndGardens/tree/master/GreenspaceRecord) umgewandelt und an die Datenplattform gesendet. Hervorzuheben ist hierbei die Georeferenzierung der Daten, damit diese im Dashboard auf einer Karte angezeigt werden könne.

Außerdem wird über die Schnittstelle des DWD der Waldbrandgefahrenindex abgerufen und in das Datenmodell [FireForestStatus](https://github.com/smart-data-models/dataModel.Forestry/blob/master/FireForestStatus/README.md) umgewandelt.

## Kontaktinformationen

Dieses Projekt wurde von der [Hypertegrity AG](https://www.hypertegrity.de/) entwickelt.

## Lizenz

Dieses Projekt wird unter der EUPL 1.2 veröffentlicht.

## Link zum Original-Repository

https://gitlab.com/urban-dataspace-platform/use_cases/integration/bodenfeuchte/nr-bodenfeuchte